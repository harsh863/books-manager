# **BOOKS MANAGER**


**To run this project:-** 

###### 1.clone this repository.

###### 2.navigate to the project folder using -> `cd books`

###### 3.install required project dependencies using -> `pipenv install`

###### 4.run `book.py` in your terminal by -> `pipenv run python book.py`  or you can just simply run it from any editor or IDE of your choice 
             
